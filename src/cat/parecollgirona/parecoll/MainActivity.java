package cat.parecollgirona.parecoll;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		WebView webview = (WebView) findViewById(R.id.web_temps);
//		String summary = "<html><body>You scored <b>192</b> points.</body></html>";
//		webview.loadData(summary, "text/html", null);
//		webview.loadUrl("http://www.eltercer.cat/");
		setContentView(R.layout.activity_main);
//		setContentView(webview);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void veureTemps(View view) {
		Intent intent = new Intent(this, DisplayTempsActivity.class); // Do something in response to button
	    startActivity(intent);
	}
	
	public void veureContacte(View view) {
		Intent intent = new Intent(this, VeureContacteActivity.class); // Do something in response to button
	    startActivity(intent);
	}
	
	public void veureCalendari(View view){
		Intent intent = new Intent(this, CalendariActivity.class);
		startActivity(intent);
		
	}
	
	public void jugarMemo(View view){
		Intent intent = new Intent(this, MemoActivity.class);
		startActivity(intent);
		
	}

	
	public void veureLogIn(View view){
		Intent intent = new Intent(this, LogInActivity.class);
		startActivity(intent);
		
	}

	static final int REQUEST_IMAGE_CAPTURE = 1;

//	public void dispatchTakePictureIntent(View view) {
//	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//	        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//	    }
//	}
//	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//	        Bundle extras = data.getExtras();
//	        Bitmap imageBitmap = (Bitmap) extras.get("data");
//	        ImageView mImageView = (ImageView) findViewById(R.id.foto);
//	        mImageView.setImageBitmap(imageBitmap);
//	    }
//	}
}
