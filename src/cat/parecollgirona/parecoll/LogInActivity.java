package cat.parecollgirona.parecoll;

import org.apache.http.util.EncodingUtils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

public class LogInActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_in);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.log_in, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void testLogIn(View view) {
		TextView textview = (TextView) findViewById(R.id.textView1);
		EditText et_nom = (EditText) findViewById(R.id.editText1);
		EditText et_pass = (EditText) findViewById(R.id.editText2);
		String nom_usuari = et_nom.getText().toString();
		String contrasenya = et_pass.getText().toString();
		textview.setText("Provarem "+nom_usuari+" amb "+contrasenya);
		WebView wv = (WebView) findViewById(R.id.webView1);
		String url = "https://fedacsantnarcis.clickedu.eu/user.php?action=doLogin";
		String postData = "username="+nom_usuari+"&password="+contrasenya;
		wv.postUrl(url,EncodingUtils.getBytes(postData, "BASE64"));
	}
	
}
