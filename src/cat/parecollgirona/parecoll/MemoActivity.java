package cat.parecollgirona.parecoll;

import java.util.Random;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;

public class MemoActivity extends Activity {
	
	private Integer[] mThumbIds = {
            R.drawable.anunciata,
            R.drawable.dominiques,
            R.drawable.gombren,
            R.drawable.logo_48x48,
            R.drawable.parecoll,
            R.drawable.santnarcis
    };
	
	private Integer[] codis = {0,1,2,3,4,5,0,1,2,3,4,5};
	
	private int targeta1;
	private ImageButton boto1;
	private int targeta2;
	private int targetes_destapades=0;
	private int targetes_descobertes = 0;
	private boolean apunt = true;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Random aleatori = new Random();
		int j;
		int temporal;

		String cadena="Ordre: ";
		
//		//posem les targetes de dos en dos		
//		for(int i=0;i<6;i++){
//			codis[i] = i;
//			codis[i+6] = i;
//		}
		
		// ara reordenarem aleatoriament la llista codis
		
		
		for(int i=0; i<12; i++){
			temporal = codis[i];
			j = aleatori.nextInt(11);
			codis[i] = codis[j];
			codis[j] = temporal;
       }
		
		for(int i=0; i<12; i++){
			cadena = cadena + "T"+codis[i]+ " ";
       }
		
		
		setContentView(R.layout.activity_memo);
		TextView textview = (TextView) findViewById(R.id.textView1);
		textview.setText(cadena);
		Chronometer chronometer = (Chronometer) findViewById(R.id.chronometer1);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
		// Show the Up button in the action bar.
		setupActionBar();
	}

	public void girarTargeta(final View view){
		TextView textview = (TextView) findViewById(R.id.textView1);
//		((ImageButton) view).setImageResource(R.drawable.ic_launcher);
		String snum=view.getTag().toString();
		int num = Integer.parseInt(snum);
		if (apunt==true){
			((ImageButton) view).setImageResource(mThumbIds[codis[num]]);
			targetes_destapades++;
			
			if (targetes_destapades==1){
				targeta1 = codis[num];
				boto1 = (ImageButton) view;
			}
			if (targetes_destapades==2){
				apunt = false;
				targeta2 = codis[num];
				targetes_destapades=0;
				if (targeta1!=targeta2){
		            view.postDelayed(new Runnable() {
		                @Override
		                public void run() {
		    				((ImageButton) view).setImageResource(R.drawable.ic_launcher);
		    				boto1.setImageResource(R.drawable.ic_launcher);
		    				apunt = true;
		                }
		            }, 500);
					
				}
				else{
					targetes_descobertes = targetes_descobertes + 2;
					textview.setText("Descobertes: "+targetes_descobertes);
					boto1.setEnabled(false);
					view.setEnabled(false);
					apunt = true;
					if (targetes_descobertes==12){
						Chronometer chronometer = (Chronometer) findViewById(R.id.chronometer1);
						chronometer.stop();
					}



				}

			}
			
		}
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.memo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
